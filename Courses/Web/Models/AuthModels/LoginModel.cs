﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.AuthModels
{
    public class LoginModel
    {
        [MaxLength(50)]
        [Required(ErrorMessage = "Email should not be empty!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [MaxLength(20)]
        [Required(ErrorMessage = "Password should not be empty!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
