﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BusinessLayer.Repositories.Courses;
using BusinessLayer.Repositories.Customers;
using BusinessLayer.Repositories.Users;
using DataLayer.Entities;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserRepository userRepository;
        private readonly ICourseRepository courseRepository;
        private readonly ICustomerRepository customerRepository;

        public HomeController(
            IUserRepository userRepository, 
            ICourseRepository courseRepository,
            ICustomerRepository customerRepository)
        {
            this.userRepository = userRepository;
            this.courseRepository = courseRepository;
            this.customerRepository = customerRepository;
        }

        public IActionResult Courses()
        {
            if (User.Identity.IsAuthenticated)
            {
                List<Course> courses = courseRepository.GetAll().ToList();

                return View(courses);
            }

            return new EmptyResult();
        }

        public IActionResult Customers()
        {
            if (User.Identity.IsAuthenticated)
            {
                List<Customer> customers = customerRepository.GetAll().ToList();

                return View(customers);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public IActionResult UserEdit(int userId)
        {
            User user = new User();

            if (userId != 0)
            {
                user = userRepository.GetById(userId);
            }

            return View(user);
        }


        [HttpPost]
        public IActionResult UserEdit(User user)
        {
            userRepository.Save(user);

            return RedirectToAction("Login", "Auth");
        }

        [HttpGet]
        public IActionResult CourseEdit(int courseId)
        {
            Course course = new Course();

            if (courseId != 0)
            {
                course = courseRepository.GetById(courseId);
            }

            return View(course);
        }

        [HttpPost]
        public IActionResult CourseEdit(Course course)
        {
            courseRepository.Save(course);

            return RedirectToAction("Courses", "Home");
        }

        public IActionResult CourseView(int courseId)
        {
            var course = courseRepository.GetById(courseId);

            return View(course);
        }

        public IActionResult CourseDelete(int courseId)
        {
            var course = courseRepository.GetById(courseId);
            courseRepository.Delete(course);

            return RedirectToAction("Courses", "Home");
        }

        //

        [HttpGet]
        public IActionResult CustomerEdit(int customerId)
        {
            Customer customer = new Customer();

            if (customerId != 0)
            {
                customer = customerRepository.GetById(customerId);
            }


            var courses = courseRepository.GetAll().ToList();
            ViewBag.Courses = courses;

            return View(customer);
        }

        [HttpPost]
        public IActionResult CustomerEdit(Customer customer)
        {
            customerRepository.Save(customer);

            return RedirectToAction("Customers", "Home");
        }

        public IActionResult CustomerDelete(int customerId)
        {
            var customer = customerRepository.GetById(customerId);
            customerRepository.Delete(customer);

            return RedirectToAction("Customers", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
