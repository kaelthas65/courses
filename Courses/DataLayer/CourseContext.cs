﻿using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataLayer
{
    public class CourseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public CourseContext(DbContextOptions<CourseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().HasIndex(a => a.Id);
            modelBuilder.Entity<Customer>().HasIndex(a => a.Id);
            modelBuilder.Entity<User>().HasIndex(a => a.Id);

            base.OnModelCreating(modelBuilder);
        }
    }
}