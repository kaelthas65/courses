﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Password { get; set; }

        [MaxLength(128)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }
    }
}
