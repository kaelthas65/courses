﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataLayer.Entities.Enums;

namespace DataLayer.Entities
{
    public class Course
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        public SubjectEnum.Subject Subject { get; set; }

        [MaxLength(256)]
        public string Description { get; set; }

        public LangEnum.Lang Language { get; set; }

        [MaxLength(50)]
        public string TeacherName { get; set; }

        public List<Customer> Customers { get; set; }
    }
}
