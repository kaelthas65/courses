﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities.Enums
{
    public class SubjectEnum
    {
        public enum Subject
        {
            [Display(Name = "Информационные системы")]
            IS,

            [Display(Name = "Искусственный интеллект")]
            AI,

            [Display(Name = "Маркетинг")]
            Marketing,

            [Display(Name = "Программирование")]
            Programming,
        }
    }
}