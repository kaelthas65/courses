﻿using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities.Enums
{
    public class LangEnum
    {
        public enum Lang
        {
            [Display(Name = "Русский")]
            Russian,

            [Display(Name = "Английский")]
            English
        }
    }
}