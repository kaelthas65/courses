﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataLayer.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(128)]
        public string FullName { get; set; }

        public DateTime BirthDate { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        public bool IsStudent { get; set; }

        public Course Course { get; set; }

        public int CourseId { get; set; }
    }
}
