﻿using DataLayer.Entities;

namespace BusinessLayer.Repositories.Users
{
    public interface IUserRepository
    {
        User GetById(int id);

        User GetByEmailAndPassword(string email, string password);

        void Save(User user);
    }
}
