﻿using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayer.Entities;

namespace BusinessLayer.Repositories.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly CourseContext context;

        public UserRepository(CourseContext context)
        {
            this.context = context;
        }

        public User GetById(int id)
        {
            return context.Users.First(x => x.Id == id);
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            return context.Users.FirstOrDefault(u => u.Email == email && u.Password == password);
        }

        public void Save(User user)
        {
            if (user.Id == 0)
                context.Users.Add(user);
            else
                context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }
    }
}
