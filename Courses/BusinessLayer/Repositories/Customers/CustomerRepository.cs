﻿using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Repositories.Customers
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly CourseContext context;

        public CustomerRepository(CourseContext context)
        {
            this.context = context;
        }

        public IList<Customer> GetAll()
        {
            return context.Customers.Include(y => y.Course).ToList();
        }

        public IList<Customer> GetByCourseId(int courseId)
        {
            return context.Customers.Include(y => y.Course).Where(x => x.CourseId == courseId).ToList();
        }

        public Customer GetById(int id)
        {
            return context.Customers.Include(y => y.Course).First(x => x.Id == id);
        }

        public void Save(Customer customer)
        {
            if (customer.Id == 0)
                context.Customers.Add(customer);
            else
                context.Entry(customer).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(Customer customer)
        {
            context.Customers.Remove(customer);
            context.SaveChanges();
        }
    }
}
