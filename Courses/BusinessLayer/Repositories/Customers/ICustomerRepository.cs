﻿using System.Collections.Generic;
using DataLayer.Entities;

namespace BusinessLayer.Repositories.Customers
{
    public interface ICustomerRepository
    {
        IList<Customer> GetAll();

        IList<Customer> GetByCourseId(int courseId);

        Customer GetById(int id);

        void Save(Customer customer);

        void Delete(Customer customer);
    }
}
