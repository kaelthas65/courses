﻿using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Repositories.Courses
{
    public class CourseRepository : ICourseRepository
    {
        private readonly CourseContext context;

        public CourseRepository(CourseContext context)
        {
            this.context = context;
        }

        public IList<Course> GetAll()
        {
            return context.Courses.Include(y => y.Customers).ToList();
        }

        public Course GetById(int id)
        {
            return context.Courses.Include(y => y.Customers).First(x => x.Id == id);
        }

        public void Save(Course course)
        {
            if (course.Id == 0)
                context.Courses.Add(course);
            else
                context.Entry(course).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(Course course)
        {
            context.Courses.Remove(course);
            context.SaveChanges();
        }
    }
}
