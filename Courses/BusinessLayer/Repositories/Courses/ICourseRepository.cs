﻿using System.Collections.Generic;
using DataLayer.Entities;

namespace BusinessLayer.Repositories.Courses
{
    public interface ICourseRepository
    {
        IList<Course> GetAll();

        Course GetById(int id);

        void Save(Course course);

        void Delete(Course course);
    }
}
